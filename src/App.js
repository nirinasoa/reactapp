import logo from './logo.svg';
import './App.css';
import Greet from './components/Greet';
import Message from './components/Message';
import Counter from './components/Counter';
import Welcome from './components/Welcome';
import FunctionClick from './components/FunctionClick';
import ClassClick from './components/ClassClick';
import EventBind from './components/EventBind';
import ParentComponent from './components/ParentComponent';
import UserGreeting from './components/UserGreeting';
import Facture from './components/Facture';
import NameList from './components/NameList';
import Stylesheet from './components/Stylesheet';
import InlineCss from './components/InlineCss';
import './appStyles.css';
import styles from './appStyles.module.css';
import Form from './components/Form';


function App() {
  return (
    <div className="App">
      <Form/>
      {/* <h1 className="err">Error</h1>
      <h1 className={styles.success}>Sucess</h1> */}
      {/* <InlineCss/> */}
      {/* <Stylesheet primary={false}></Stylesheet> */}
      {/* <NameList/> */}
      {/* <Facture/> */}
      {/* <ParentComponent/> */}
      {/* <EventBind/> */}
      {/* <ClassClick/> */}
        {/* <FunctionClick/> */}
        {/* <Greet name="Yael" pseudo="Soasoa">
          <p>Hey o</p>
          </Greet>
        <Greet name="Levi" pseudo="Levilia"></Greet> */}
        {/* <Welcome name="Yael" pseudo="Soasoa" >
        <p>Hey o</p>
        </Welcome>
        <Welcome name="Levi" pseudo="Levilia" /> */}

        {/* <Message/> */}
        {/* <Counter/> */}
      
    </div>
  );
}

export default App;
