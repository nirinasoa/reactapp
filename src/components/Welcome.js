import React,{Component} from 'react';
class Welcome extends Component{
    render(){
        const {name,pseudo}=this.props;
        return(
           <h1>Hello {name} {pseudo} </h1>
        )
    }
}
export default Welcome;