import React from'react';

// function Greet(){
//     return <h1>Hello Yael</h1>
// }
//Greet function with es6
///const Greet=(props)=>{
    //destructuring in the parameters bame,pseudo
    //const Greet=({name,pseudo})=>{
    //destructuring in the function body
    const Greet=(props)=>{
        const {name,pseudo}=props
        return(
            <div>
                <h1>Hello {name}  {pseudo} </h1>
            
            </div>
            )
}

export default Greet;