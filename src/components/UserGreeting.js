import React, { Component } from 'react'

 class UserGreeting extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
              isLoggedIn:false
         }
     }
     
    render() {
        return(
            this.state.isLoggedIn ?
            <div>Welcome Yael</div>:
            <div>Welcome Guest</div>
        )
        //Tenary conditional operator
        // let message
        // if(this.state.isLoggedIn){
        //        message=<h4>Welcome Yael</h4>
        // }
        // else{
        //     message=<h4>Welcome Guest</h4>
        // }
        // return <div>{message}</div>

        // if(this.state.isLoggedIn){
        //     return (
        //         <h4>Welcome Yael</h4>
        //     )
        // }
        // else{
        //     return (
        //         <h4>Welcome Guest</h4>
        //     ) 
        // }
        // return (
        //     <div>
        //        <h4>Welcome Yael</h4>
        //        <h4>Welcome Guest</h4>
        //     </div>
        // )
    }
}

export default UserGreeting
