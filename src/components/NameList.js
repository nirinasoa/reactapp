import React from 'react'
import Person from './Person'

function NameList() {
    const names=['Yael','Levi','Ezaraia']
    const persons=[
        {
            id:1,
            name:'yael',
            age:22,
        },
        {
            id:2,
            name:'Levi',
            age:25,
        },
        {
            id:3,
            name:'Ezaraia',
            age:5,
        }
    ]
    // const nameList= names.map(name=><h2>{name}</h2> )
    //the list component is only responsable for rendering the list
    const personList= persons.map((person,index)=><Person keyPerso={person.id}  key={index} person={person}></Person>)
    return (
        <div>
            {/* <h2>{names[0]}</h2> 
            <h2>{names[1]}</h2> 
            <h2>{names[2]}</h2>  */}
            {
                personList
            }
        </div>
    )
}

export default NameList
