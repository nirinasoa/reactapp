import React from 'react'

function Person({person,keyPerso}) {
    return (
        // Person class iS responsable for rendering the person HTML
        <div>
            <h2>{keyPerso} I am {person.name}. I am {person.age}</h2>
        </div>
    )
}

export default Person
